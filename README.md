MFRC522-python
==============

A small class to interface with the NFC reader Module MFRC522 on the NanoPi Neo.

This is a Python port of the example code for the NFC module MF522-AN.

##Requirements

This code requires you to have SPI-Py installed from the following repository:

https://github.com/lthiery/SPI-Py

And GPIO installed from:

https://github.com/vitiral/gpio

or with pip

pip3 install gpio

##Examples
This repository includes a couple of examples showing how to read, write, and dump data from a chip. They are thoroughly commented, and should be easy to understand.

## Pins
You can use [this](http://nanopi.io/nanopi-neo.html) for reference.

| Name | Pin # | Pin name   |
|------|-------|------------|
| SDA  | 24    | GPIO67     |
| SCK  | 23    | GPIO93     |
| MOSI | 19    | GPIO64     |
| MISO | 21    | GPIO65     |
| IRQ  | None  | None       |
| GND  | 20    | Any Ground |
| RST  | 22    | GPIO1      |
| 3.3V | 17    | 3V3        |

##Usage
Import the class by importing MFRC522 in the top of your script. For more info see the examples.
