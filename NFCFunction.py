#!/usr/bin/env python
# -*- coding: utf8 -*-

import MFRC522
import signal
import Function
class NFCFunction:
    
    # Capture SIGINT for cleanup when the script is aborted
    def end_read(signal,frame):
        global continue_reading
        print ("Ctrl+C captured, ending read.")
        continue_reading = False
        GPIO.cleanup()
    signal.signal(signal.SIGINT, end_read)
    MIFAREReader = MFRC522.MFRC522()

    def read_NFC(self,NFC_DETECTED_FLAG,END_FLAG):
        if NFC_DETECTED_FLAG==False:
            data=""
            # Scan for cards    
            (status,TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)
            # If a card is found
            if status == self.MIFAREReader.MI_OK:   
                # Get the UID of the card
                (status,uid) = self.MIFAREReader.MFRC522_Anticoll()      
                # If we have the UID, continue
                if status == self.MIFAREReader.MI_OK:    
                    # This is the default key for authentication
                    key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]         
                    # Select the scanned tag
                    self.MIFAREReader.MFRC522_SelectTag(uid)
                    # Authenticate
                    status = self.MIFAREReader.MFRC522_Auth(self.MIFAREReader.PICC_AUTHENT1A, 8, key, uid)
                    # Check if authenticated
                    if status == self.MIFAREReader.MI_OK:
                        data=self.MIFAREReader.MFRC522_Read(8)
                        if END_FLAG==True:
                            self.MIFAREReader.MFRC522_StopCrypto1()
                        return(' '.join(str(e) for e in uid),data)
                    else:
                        print ("Authentication error")
        return ("","")               
    
    def write_NFC(self,uid_NFC,dataWrite):
        uid_NFCList=Function.strToList(uid_NFC)
        print(dataWrite)
        dataWriteList=Function.strToList(dataWrite)
        print(str(dataWriteList))
        # Scan for cards    
        (status,TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)
        # If a card is found
        if status == self.MIFAREReader.MI_OK:   
            # Get the UID of the card
            (status,uid) = self.MIFAREReader.MFRC522_Anticoll()      
            # If we have the UID, continue
            if status == self.MIFAREReader.MI_OK:    
                # This is the default key for authentication
                key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]         
                # Select the scanned tag
                self.MIFAREReader.MFRC522_SelectTag(uid)
                # Authenticate
                status = self.MIFAREReader.MFRC522_Auth(self.MIFAREReader.PICC_AUTHENT1A, 8, key, uid)
                # Check if authenticated
                if status == self.MIFAREReader.MI_OK:
                    data=self.MIFAREReader.MFRC522_Read(8)
                    if cmp(uid,uid_NFCList)==0:
                        self.MIFAREReader.MFRC522_Write(8, dataWriteList) 
                        self.MIFAREReader.MFRC522_StopCrypto1() 
                        return True 
                else:
                    print ("Authentication error")
        return False